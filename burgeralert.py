import asyncio
import requests
from bs4 import BeautifulSoup
import discord
from discord.ext import tasks, commands

intents = discord.Intents.default()
intents.members = True

client = commands.Bot(command_prefix='!', intents=intents)
TOKEN = ''
channel_id = 

@client.event
async def on_ready():
    channel = client.get_channel(channel_id)
    await channel.send(f'{client.user.name} has connected to Discord!')
    check_burger.start()
    clear_messages.start()

@tasks.loop(hours=24)
async def check_burger():
    urls = {
        'studentska': 'https://agata.suz.cvut.cz/jidelnicky/?clPodsystem=2',
        'technicka': 'https://agata.suz.cvut.cz/jidelnicky/?clPodsystem=3'
    }
    burger_in_both = False
    for name, url in urls.items():
        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        if 'burger' in soup.text:
            if name == 'studentska':
                message = f'VE STUDENTSKÝ MENZE JE DNES BURGER.'
                if 'burger' in urls['technicka']:
                    message += '\nA JE TAKÉ V TECHNICKÝ!'
                    burger_in_both = True
            elif name == 'technicka':
                message = f'V TECHNICKÝ MENZE JE DNES BURGER.'
                if 'burger' in urls['studentska']:
                    message += '\nA JE TAKÉ VE STUDENTSKÝ!'
                    burger_in_both = True
            channel = client.get_channel(channel_id)
            await channel.send(message)
    if burger_in_both:
        message = 'BURGER JE DNES V OBOU MENZÁCH!'
        channel = client.get_channel(channel_id)
        await channel.send(message)

@tasks.loop(hours=12)
async def clear_messages():
    channel = client.get_channel(channel_id)
    await channel.purge()

@check_burger.before_loop
async def before_check_burger():
    await client.wait_until_ready()
    # delete previous message if there is any
    channel = client.get_channel(channel_id)
    await channel.purge(limit=1, check=lambda m: m.author == client.user)


async def main():
    await client.start(TOKEN)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
